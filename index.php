<?php

session_start();

///**Данные для входа в mysql**
$db = [
    'host' => 'localhost',
    'name' => 'root',
    'password' => 'root',
    'dbname' => 'users'
];
///**Создание подключения mysql**
$dbconnect = new PDO("mysql:host={$db['host']};dbname={$db['dbname']}", $db['name'], $db['password']);

///**Берём URI и вытаскиваем login или logout**
$uri = trim($_SERVER['REQUEST_URI'], "/");
preg_match("/[a-z]+/", $uri, $request);

///**Форма может быть еще не введена, поэтому ставим @ перед определением переменной**
@$login = $_POST['login'];
@$password = $_POST['password'];

///**Шапка сайта**
include_once $_SERVER['ROOT'] . 'header.php';
//**Логика входа**
if ($request[0] === "login") {
    if (isset($_SESSION['login'])) {
        show_content();
    } else {
        if (!(check_error() === "")) {
            show_form(check_error());
        } else {
            set_session();
            show_content();
        }
    }
} elseif ($request[0] === "logout") {
    unset_session();
    show_logout();
} else {
        show_form(null);
}
///**Подвал сайта**
include_once $_SERVER['ROOT'] . 'footer.php';

function check_error() { ///**Проверка на ошибки при вводе**
    global $login, $password;

    $errors = "";

    if ($login === "") {
        $errors .= "<div class=\"alert alert-danger\">Вы не заполнини поле <strong>Login</strong></div>";
    } elseif (strlen($login) < 4) {
        $errors .= "<div class=\"alert alert-danger\">Поле <strong>Login</strong> должно содержать не менее 4 символов</div>";
    } elseif (strlen($login) > 20) {
        $errors .= "<div class=\"alert alert-danger\">Поле <strong>Login</strong> должно содержать не более 20 символов</div>";
    } else {
        $errors .= check_login();
    }


    if ($password === "") {
        $errors .= "<div class=\"alert alert-danger\">Вы не заполнини поле <strong>Password</strong></div>";
    } elseif (strlen($password) < 6) {
        $errors .= "<div class=\"alert alert-danger\">Поле <strong>Password</strong> должно содержать не менее 6 символов</div>";
    } elseif (strlen($login) > 35) {
        $errors .= "<div class=\"alert alert-danger\">Поле <strong>Password</strong> должно содержать не более 35 символов</div>";
    } else {
        $errors .= check_password();
    }

    return $errors;
}

function check_login() { ///**Есть ли такой логин в базе?**
    global $login, $dbconnect;

    $sql = 'SELECT COUNT(*) from users WHERE login = ? LIMIT 1';
    $stmt = $dbconnect->prepare($sql);
    $stmt->bindParam(1, trim($login), PDO::PARAM_STR);
    $stmt->execute();

    if (!($stmt->fetchColumn())) return "<div class=\"alert alert-danger\">Такого <strong>Login</strong> не существует</div>";

    return "";
}

function check_password() { ///**Правильный пароль?**
    global $login, $password, $dbconnect;
    //$login = md5($login);

    $sql = 'SELECT * FROM users WHERE login = ? AND password = ? LIMIT 1';
    $stmt = $dbconnect->prepare($sql);
    $stmt->bindParam(1, $login, PDO::PARAM_STR);
    $stmt->bindParam(2, $password, PDO::PARAM_STR);
    $stmt->execute();

    if (!($stmt->fetchColumn())) return "<div class=\"alert alert-danger\">Неверный пароль</div>";

   return "";
}

function set_session() { ///**Определяем переменную в сессии**
    global $login;
    $login = htmlspecialchars(trim($login));

    $_SESSION['login'] = $login;
}

function unset_session() { ///**Выходим**
    unset($_SESSION['login']); 
}

function show_form($errors) { ///**Сама форма входа**
    global $login;
    print <<<FORM
            <div class="container">{$errors}</div>
            <form class="form-inline" method="POST" action="?login">
                <div class="form-group">
                    <label for="login">Login:</label>
                    <input type="login" class="form-control" id="login" name="login" value="{$login}">
                </div>
                <div class="form-group">
                    <label for="pswd">Password:</label>
                    <input type="password" class="form-control" id="pswd" name="password">
                </div>
                <input type="submit" class="btn btn-default" value="Submit" name="submit">
            </form>
        </div>
FORM;
}

function show_content() { ///**Контент, который показывается при успешном входе**
    print <<<SHOWCONTENT
    Добрый день, {$_SESSION['login']}. <p>Может быть вы хотите <form action="?logout" method="POST"><input type="submit" value="выйти?" class="btn btn-danger"></form>
SHOWCONTENT;
   }

function show_logout() { ///**Контент, который показывается при успешном выходе**
    print "Вы вышли! Может быть вы хотите <form action=\"?\"><input type=\"submit\" value=\"зайти?\" class=\"btn btn-danger\"></form>";
}
