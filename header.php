<!--- 
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
);
--->


<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Login.</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Anonymous+Pro|Roboto+Condensed" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <style>
        .container-fluid {
            padding-top: 140px;
            padding-bottom: 140px;
            font-family: 'Anonymous Pro', monospace;
        }

        .bg-1 { 
            background-color: #b4d0e9; /* Green */
            color: #ffffff;
        }

        .bg-2 { 
            background-color: #f3d2b3; /* Green */
            color: #ffffff;
        }

        .bg-3 { 
            background-color: #436d89; /* Green */
            color: #ffffff;
        }

    </style>

    <body>
        <div class="container-fluid bg-1 text-center">
            <h1>Form</h1>
        </div>
        <div class="container-fluid bg-2 text-center">

